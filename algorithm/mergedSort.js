const arr = [5, 4, 3, 2, 1];

const merge = (left, right) => {
  let result = [];
  let lIdx = 0;
  let rIdx = 0;

  // 兩邊都有剩餘，要進行比較
  while (lIdx < left.length && rIdx < right.length) {
    if (left[lIdx] < right[rIdx]) {
      result.push(left[lIdx]);
      lIdx++;
    } else {
      result.push(right[rIdx]);
      rIdx++
    }
  }

  // 左邊有剩下，表示通通比剛剛的大，全部推進去ㄓ
  while (lIdx < left.length) {
    result.push(left[lIdx]);
    lIdx++;
  }

  // 右邊有剩下，表示通通比剛剛的大，全部推進去ㄓ
  while (rIdx < right.length) {
    result.push(right[rIdx]);
    rIdx++
  }

  return result;
}

const mergedSort = (list) => {
  if (list.length === 1) {
    return list;
  }

  const len = list.length
  const midIndex = Math.floor(len / 2);
  const left = list.slice(0, midIndex);
  const right = list.slice(midIndex, len);

  return merge(mergedSort(left), mergedSort(right))
}

console.log(mergedSort(arr))