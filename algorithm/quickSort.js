const arr = [3, 5, 2, 1, 4];

const quickSort = (list) => {
  const len = list.length
  if (len < 2) {
    return list;
  }

  const pivot = list[0];
  console.log('pivot', pivot)
  const left = [];
  const right = [];

  for (let i = 1; i < len; i++) {
    const currNum = list[i]
    if (pivot > currNum) {
      left.push(currNum)
    } else {
      right.push(currNum)
    }
  }

  return [...quickSort(left), pivot, ...quickSort(right)]
}

console.log(quickSort(arr))