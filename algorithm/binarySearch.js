const arr = [];

for (let i = 1; i <= 100; i++) {
  arr.push(i)
}

const binarySearch = (list, target) => {
  let seachCount = 0;
  let low = 0;
  let high = arr.length - 1;

  while (low <= high) {
    seachCount += 1;
    const mid = Math.floor((low + high) / 2);
    const guess = list[mid];
    if (guess === target) {
      console.log('searchCount:', seachCount)
      return mid;
    }

    if (guess > target) {
      high = mid - 1
    } else {
      low = mid + 1
    }

  }

  return
}

console.log(binarySearch(arr, 50))