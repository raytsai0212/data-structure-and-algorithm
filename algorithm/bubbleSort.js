const arr = [5, 4, 3, 2, 1];

const bubbleSort = (list) => {
  const swap = (index1, index2) => {
    const temp = list[index1];
    list[index1] = list[index2];
    list[index2] = temp;
  }

  for (let i = 0; i < list.length; i++) {
    for (let j = 0; j < list.length - i - 1; j++) {
      if (arr[j] > arr[j + 1]) {
        swap(j, j + 1)
      }
    }
  }

  return list;
}

console.log(bubbleSort(arr))