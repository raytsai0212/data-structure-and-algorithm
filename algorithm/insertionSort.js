const arr = [5, 4, 3, 2, 1]

function insertionSort(list) {

  for (let i = 1; i < list.length; i++) {
    let temp = list[i];
    let j = i;

    while (j > 0 && list[j - 1] > temp) {
      list[j] = list[j - 1];
      j--;
    }
    list[j] = temp;
    console.log(list)
  }

  return arr
}

console.log(insertionSort(arr))