const randomArr = [25, 31, 1, 4, 52];
const randomArr2 = [5, 4, 3, 2, 1];


const findSmallestIndex = (list) => {
  let smallestNum = list[0];
  let smallestIndex = 0;

  for (let i = 1; i < list.length; i += 1) {
    if (list[i] < smallestNum) {
      smallestNum = list[i];
      smallestIndex = i;
    }
  }

  return smallestIndex
}

const selectionSearch = (list) => {
  const newList = [];
  const len = list.length;
  for (let i = 0; i < len; i++) {
    const smallestIndex = findSmallestIndex(list);
    newList.push(list[smallestIndex]);
    list.splice(smallestIndex, 1);
  }

  return newList;
}

console.log(selectionSearch(randomArr));
console.log(selectionSearch(randomArr2))